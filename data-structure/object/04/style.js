< !DOCTYPE html >
	< html lang = "en" >
	< head >
	< meta charset = "UTF-8" >
	< title > Document < /title> < /head > < body >
	< script >
	"use strict";

function doBig(st) { //calculate Big chars
	var res = 0;
	for (var i = 0; i < st.length; i++) {
		if (st[i] == st[i].toUpperCase()) {
			res++;
		}
	}
	return res;
}

function doSmall(st) { //calc Small chars
	return st.length - doBig(st);
}

function changeStringInArr(arr) {
	return arr.map(function(v) {
		if (doBig(v) > doSmall(v)) {
			return v.toUpperCase();
		} else {
			return v.toLowerCase();
		}
	})
}

var useAndrei = prompt("Enter elements ','").split(',');

alert(changeStringInArr(useAndrei));

< /script> < /body >
< /html>